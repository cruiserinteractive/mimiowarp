/* $Header: /home/tapted/cvstemp/cvs/Nightingale/mimiowarp/mimiocal.cc,v 7.2 2005/03/10 01:51:27 tapted Exp $ */

/**\file mimiocal.cc
 * A screen calibration program using / demonstrator of
 * an exact-interpolation radial basis function neural network.
 *
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * \version $Revision: 7.2 $
 * \date $Date$
 */


#include <SDL.h> //for SDL_Surface, events, etc.
#include <SDL_thread.h>
#include <unistd.h>  //for size_t, usleep

#include <deque>    //for points queue
#include <vector>
#include <utility>  //for std::pair
#include <fstream>
#include <iostream>

#include <stdio.h>

#include "sdl_server.h"
SDL_SERVER_DECLARECLIENT(MimioMonitor);

/** define VDEBUG for verbose debugging in rbn_solve.h */
#define VDEBUG
#undef VDEBUG
//#define DEBUG
//#undef DEBUG

#include "exactrbn/src/rbn_solve.h"
#include "strokereader.h"

namespace {
/** Dump a MimioEvent */
void dumpme(MimioEvent *e) {
    fprintf(stderr, "MimioEvent = {%s, %d, (%d, %d)}\n",
            e->state == MimioEvent::UP ?       "     UP" :
            (e->state == MimioEvent::DOWN ?    "   DOWN" :
             (e->state == MimioEvent::MOVING ? " MOVING" :
              "NOTHING")),
            e->user, e->x, e->y);
}
}

using std::make_pair;

//static int mimiocal = fprintf(stderr, "mimiocal init\n");

/* Configuration namespace */
namespace Config {
    int TARGET_SCREEN_WIDTH = 1024; ///< Width of the SDL surface
    int TARGET_SCREEN_HEIGHT = 768; ///< Height of the SDL Surface
    int BOX_SIZE = 20;       ///< Width of the calibration squares
    int TARGET_SCREEN_BPP = 24; ///< Colour depth of the SDL Surface
    int DIFF = 32;           ///< Distance between calibration squares
    float FROM_EDGE = 0.05;  ///< Portion from edge of screen at which calibrations squares are drawn
    //bool FULLSCREEN = true;  ///< Whether to enter fullscreen mode
    bool FULLSCREEN = true;  ///< Whether to enter fullscreen mode
    bool SHOWPIXELS = true;  ///< Whether to show mouse movement events to stderr
    unsigned MAXDIM = 8;          ///< Maximum number of calibration points
    double SIGMA = 2.5;      ///< Value of sigma for the RBF Network

    int MIMIO_WIDTH = 9600; ///< Width of the Mimio coordinates
    int MIMIO_HEIGHT = 4800; ///< Height of the Mimio coordindates

    int POINTS_TO_DISCARD = 10; ///< How many mimio points to discard
    int POINTS_TO_AVERAGE = 40; ///< How many mimio points to average for each calibration point

    const char* MIMIO_HOST = "127.0.0.1"; ///< The host computer, with the mimio, or the device node, or 'mouse'

    SDL_Surface * background = 0; ///< The background image to blit
    SDL_sem * mouse_sem = 0;      ///< Semaphore used to block, waiting for mouse emulation
    int last_mouseclick_x = 0;    ///< Last mouse click x-pos
    int last_mouseclick_y = 0;    ///< Last mouse click y-pos
    bool mouse_emu = false;       ///< Whether we are doing mouse emulation

    SDL_Surface *surf = 0; ///< The SDL surface / screen display handle

    /** Render a background, so that the projector can calibrate properly */
    void blit_back() {
        if (background && surf) {
            //SDL_LockSurface(surf);
            SDL_Rect sr = {0, 0, 0, 0};
            SDL_Rect dr = sr;
            do {
                dr.w = sr.w = background->w;
                dr.h = sr.h = background->h;
                if (sr.x + sr.w > surf->w)
                    dr.w = sr.w = surf->w - sr.x;
                if (sr.y + sr.h > surf->h)
                    dr.h = sr.h = surf->h - sr.x;
                fprintf(stderr, "Blitting (%d, %d)+(%dx%d) -> (%d, %d)+(%dx%d)\n",
                        sr.x, sr.y, sr.w, sr.h, dr.x, dr.y, dr.w, dr.h);
                SDL_BlitSurface(background, &sr, surf, &dr);
                dr.x += dr.w;
                if (dr.x >= surf->w) {
                    dr.x = 0;
                    dr.y += dr.h;
                }
            } while (dr.y < surf->h);
            //SDL_UnlockSurface(surf);
        }
    }

}
using namespace Config;

/**\def LATTICEPATTERN
 * define LATTICEPATTERN to produce a line grid, rather than dots
 */
#define LATTICEPATTERN

static SDL_Surface *makeSurface(); ///< make/initialise/display the window


/** No longer used -- use waitRecentMimio instead */
static void clearTouch() {}

/** terminator variable -- set to true when we have been cancelled */
static volatile bool terminate = false;

/**
 * The "thread" that waits for SDL events, e.g. if the calibration is cancelled.
 * \note this must run in the main thread
 */
static int SDLQueueThread(void */*data*/) {
    SDL_Event event;
    fprintf(stderr, "SDLQueueThread: waiting for keyboard events to cancel\n");
    while (!terminate && SDL_WaitEvent(&event)) {
        //fprintf(stderr, "SDLQueueThread: got an event.. processing... ");
        switch(event.type) {
        case SDL_QUIT:
            fprintf(stderr, "WM Quit. Terminating.\n");
            terminate = true;
            break;
        case SDL_MOUSEMOTION:
            continue;
        case SDL_MOUSEBUTTONUP:
            if (mouse_emu && mouse_sem) {
                last_mouseclick_y = static_cast<int>
                    (1.0*event.button.y*MIMIO_HEIGHT/TARGET_SCREEN_HEIGHT + 0.5);
                last_mouseclick_x = static_cast<int>
                    (1.0*event.button.x*MIMIO_WIDTH/TARGET_SCREEN_WIDTH + 0.5);
                SDL_SemPost(mouse_sem);
            }
            break;
        case SDL_KEYDOWN:
        case SDL_KEYUP:
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                fprintf(stderr, "Escape pressed. Terminating.\n");
                terminate = true;
            } else {
                fprintf(stderr, "Ignored keyup event.\n");
            }
            break;
        case SDL_VIDEOEXPOSE:
            fprintf(stderr, "Received video expose event\n");
        default:
            fprintf(stderr, "Ingored event type %d.\n", event.type);
        }
        fprintf(stderr, "Waiting for another event\n");
    }
    fprintf(stderr,
            "SDLQueueThread: Event loop exiting. Terminate is %s. Calling cleanupMimioReader().\n",
            terminate ? "true" : "false");
    if (mouse_emu) {
        SDL_SemPost(mouse_sem);
    } else {
        cleanupMimioReader();
    }
    //fprintf(stderr, "SDLQueueThread: cleanupMimioReader() finished\n");
    return 0;
}

/** Retrieve the next calibration coordinate (in mimio coordinates) */
static bool getTouch(int &row, int &col) {
    double avgx, avgy;
    if (mouse_emu) {
        SDL_SemWait(mouse_sem);
        row = last_mouseclick_y;
        col = last_mouseclick_x;
        return true;
    }
    int ctr = 1;
    MimioEvent *me = waitRecentMimio();
    if (!me)
        return false;
    dumpme(me);
    delete me;
    me = 0;

    while (!terminate && (me = waitOldMimio()) && ctr < POINTS_TO_DISCARD) {
        dumpme(me);
        if (me->state == MimioEvent::MOVING) {
            ++ctr;
        }
	if (ctr < POINTS_TO_DISCARD) {
            delete me;
	    me = 0;
	}
    }

    if (!me)
	return false;

    avgx = me->x;
    avgy = me->y;
    delete me;
    me = 0;

    ctr = 1;
    while (!terminate && (me = waitOldMimio()) && ctr < POINTS_TO_AVERAGE) {
        dumpme(me);
        if (me->state == MimioEvent::MOVING) {
            avgx = (avgx*ctr + me->x) / (ctr+1);
            avgy = (avgy*ctr + me->y) / (ctr+1);
            ++ctr;
        }
        delete me;
    }
    if (!me)
        return false;
    row = static_cast<int>(avgy + 0.5);
    col = static_cast<int>(avgx + 0.5);
    return true;
}

/** Add a point to the callibration */
static bool doPoint (const double x, const double y, XYInterp &ts) {
    if (ts.dim() == MAXDIM)
        return false;
    SDL_LockSurface(surf);

    static std::vector<SDL_Rect> points; ///< Remeber past points for display

    const Uint32 BLACK = SDL_MapRGBA(surf->format, 0, 0, 0, 255);
    const Uint32 CYAN = SDL_MapRGBA(surf->format, 0, 255, 255, 255);
    const Uint32 YELLOW = SDL_MapRGBA(surf->format, 255, 255, 0, 255);
    const Uint32 RED = SDL_MapRGBA(surf->format, 255, 0, 0, 255);
    const Uint32 MAGENTA = SDL_MapRGBA(surf->format, 255, 0, 255, 255);

    SDL_Rect r, target;
    int tr, tc;
    target.w = BOX_SIZE;
    target.h = BOX_SIZE;

    XYInterp::EXAMPLE ex;

    ex.out[0] = 1.0 * static_cast<Sint16>(x + 0.5) / surf->w;
    ex.out[1] = 1.0 * static_cast<Sint16>(y + 0.5) / surf->h;

    SDL_Rect rr;
    rr.x = static_cast<Sint16>(x + 0.5) - 1;
    rr.y = static_cast<Sint16>(y + 0.5) - 1;
    rr.w = 3;
    rr.h = 3;

    target.x = static_cast<Sint16>(x - (BOX_SIZE / 2) + 0.5);
    target.y = static_cast<Sint16>(y - (BOX_SIZE / 2) + 0.5);

    for (std::vector<SDL_Rect>::iterator it = points.begin(); it!=points.end();++it)
        SDL_FillRect(surf, &*it, static_cast<Uint32>(MAGENTA));
    SDL_UnlockSurface(surf);
    r.x = r.y = 0;
    r.w = surf->w;
    r.h = surf->h;
    SDL_UpdateRects(surf, 1, &r);
    //SDL_UpdateRects(surf, 1, &r);

    points.push_back(rr);

    clearTouch();
    if (!terminate && !mouse_emu)
        SDL_Delay(700); //sleep 1 second before drawing the next target
    if (terminate)
        return false;
    //clearTouch();
    SDL_LockSurface(surf);
    SDL_FillRect(surf, &target, static_cast<Uint32>(CYAN));
    SDL_UnlockSurface(surf);
    SDL_UpdateRects(surf, 1, &target);

    if (!getTouch(tr, tc))
        return false;

    //draw the box red, to say we are ignoring
    SDL_LockSurface(surf);
    SDL_FillRect(surf, &target, static_cast<Uint32>(RED));
    SDL_UnlockSurface(surf);
    SDL_UpdateRects(surf, 1, &target);

    ex.in[0] = 1.0 * tc / MIMIO_WIDTH;
    ex.in[1] = 1.0 * tr / MIMIO_HEIGHT;

    r.x = r.y = 0;
    r.w = surf->w;
    r.h = surf->h;
    SDL_LockSurface(surf);
    SDL_FillRect(surf, &r, (Uint32)BLACK);
    //for (std::vector<SDL_Rect>::iterator it = points.begin(); it!=points.end();++it)
    //    SDL_FillRect(surf, &*it, reinterpret_cast<Uint32>(RED));
    SDL_UnlockSurface(surf);
    //SDL_UpdateRects(surf, 1, &r);
    ts.addExample(ex);
    ts.solve();
    fprintf(stderr, "\n(%f, %f) <- (%f, %f)\n", ex.in[0], ex.in[1], ex.out[0], ex.out[1]);
    ts.sub(ex.out, ex.in);
    fprintf(stderr, "(%f, %f) -> (%f, %f)\n", ex.in[0], ex.in[1], ex.out[0], ex.out[1]);
    r.w = r.h = 1;
    for (r.x = DIFF/2; r.x < surf->w; r.x+=DIFF) {
        for (r.y = DIFF/2; r.y < surf->h; r.y+=DIFF) {
#ifdef LATTICEPATTERN
            int mx = r.x+DIFF;
            int my = r.y+DIFF;
            for (int iter = 0; iter < 2; ++iter) {
                for (; (iter?r.x:r.y) < (iter?mx:my); (iter?r.x:r.y)++) {
#endif
            XYInterp::INVEC in;
            in[0] = 1.0*r.x/surf->w;
            in[1] = 1.0*r.y/surf->h;
            XYInterp::OUTVEC out;
            ts.sub(out, in);
            //fprintf(stderr, "(%f, %f) -> (%f, %f)\n", in[0], in[1], out[0], out[1]);
            SDL_Rect r2 = r;
            r2.x = static_cast<Sint16>(surf->w*out[0] + 0.5);
            r2.y = static_cast<Sint16>(surf->h*out[1] + 0.5);

            SDL_FillRect(surf, &r2, static_cast<Uint32>(YELLOW));
#ifdef LATTICEPATTERN
                }
                r.x = mx-DIFF;
                r.y = my-DIFF;
            }
#endif
        }
    }
    r.x = r.y = 0;
    r.w = surf->w;
    r.h = surf->h;

    //sleep half a second with the target still red
    if (!terminate)
        SDL_Delay(300);
    if (terminate)
        return false;
    SDL_UpdateRects(surf, 1, &r);
    return true;
}


/** A region of 2d space, splittable into quadrants */
struct Quad {
    //@{
    /// The region
    double xmin, xmax, ymin, ymax, xhalf, yhalf;
    //@}

    /** Constructor */
    Quad(double xn, double xx, double yn, double ym)
        :
    xmin(xn), xmax(xx), ymin(yn), ymax(ym), xhalf((xmin+xmax)*0.5), yhalf((ymin+ymax)*0.5)
    {

    }
    /** North West region */
    Quad nw() const {return Quad(xmin, xhalf, ymin, yhalf);}
    /** North East region */
    Quad ne() const {return Quad(xhalf, xmax, ymin, yhalf);}
    /** South East region */
    Quad se() const {return Quad(xhalf, xmax, yhalf, ymax);}
    /** South West region */
    Quad sw() const {return Quad(xmin, xhalf, yhalf, ymax);}
};

enum {
    W = 0x1, ///< West
    N = 0x2, ///< North
    E = 0x4, ///< East
    S = 0x8  ///< South
};


/** Pending queue of points to "do" */
static std::deque<std::pair<Quad, int> > points;

/**
 * Arbitrarily divide up a rectangular region / Quad into subregions and queue
 * points at the centre the midpoints of the edges, such that no point is
 * ever done twice.
 */
static void divide(XYInterp &ts, const Quad &q, int quads = 0xf) {
    if (quads & W)
        doPoint(q.xmin, q.yhalf, ts);
    if (quads & N)
        doPoint(q.xhalf, q.ymin, ts);
    if (quads & E)
        doPoint(q.xmax, q.yhalf, ts);
    if (quads & S)
        doPoint(q.xhalf, q.ymax, ts);

    doPoint(q.xhalf, q.yhalf, ts);

    if (points.size()*4 + ts.dim() < MAXDIM) {
        points.push_back(make_pair(q.nw(), quads&(N|S|E|W)));
        points.push_back(make_pair(q.ne(), quads&(N|E|S)));
        points.push_back(make_pair(q.se(), quads&(E|S)));
        points.push_back(make_pair(q.sw(), quads&(W|E|S)));
    }
}

/** Print out the usage message */
static void usage(const char* argv0) {
    fprintf(stderr, "Usage: %s [<width=%d> <height=%d> [sigma=%f [numpoints=%d [mimio_host=%s] ] ] ]\n",
            argv0,
            TARGET_SCREEN_WIDTH,
            TARGET_SCREEN_HEIGHT,
            SIGMA,
            MAXDIM,
            MIMIO_HOST);
    fprintf(stderr,
            "mimio_host can be:\n"
            "\t* an IP adresss or hostname to 'signal';\n"
            "\t* a device node to open (e.g. /dev/input/event2); or\n"
            "\t* the string 'mouse' to use mouse emulation (for testing)\n");
}


/** Carry out the callibration process, after reading command line arguments */
int doCalibrate() {
    fprintf(stderr, "Connected to %s.\n", MIMIO_HOST);

//    if (MIMIO_HOST[0] != '/')
    if (!surf)
        surf = makeSurface();
    if (!surf) {
        return 2;
    }
    //wait a bit for the surface to settle
    fprintf(stderr, "Waiting for surface to settle...");
    SDL_Delay(500);
    fprintf(stderr, " OK\n");
    blit_back();
    fprintf(stderr, "Waiting for surface to settle...");
    SDL_Delay(500);
    fprintf(stderr, " OK\n");

//    if (argc > 1 && strcmp(argv[1], "-t") == 0)
//        return replay();

    /* four corners */
    XYInterp ts(SIGMA);
    //XYInterp ts("network.dat");
    double xmin = surf->w*FROM_EDGE;
    double xmax = surf->w - surf->w*FROM_EDGE;
    double ymin = surf->h*FROM_EDGE;
    double ymax = surf->h - surf->h*FROM_EDGE;

    terminate || doPoint(xmin, ymin, ts);
    terminate || doPoint(xmax, ymin, ts);
    terminate || doPoint(xmax, ymax, ts);
    terminate || doPoint(xmin, ymax, ts);

    /* midpoints */
    points.push_back(make_pair(Quad(xmin, xmax, ymin, ymax), N|S|E|W));

    while (!terminate && !points.empty()) {
        divide(ts, points.front().first, points.front().second);
        points.pop_front();
    }

    if (ts.dim() >= 4) {
        std::ofstream f("network.dat");
        f << ts << std::endl;
    } else {
        fprintf(stderr,
                "Cowardly refusing to write a network with less than 4 calibration points\n"
                "The old network looks like this:\n");
    }
    {
        std::ifstream f("network.dat");
        XYInterp xyi(f);
        std::cerr << xyi << std::endl;
    }

    //SDL_Delay(1000);
    terminate = true;
    SDL_UserEvent ev = {SDL_USEREVENT, 0, 0, 0};
    SDL_PushEvent((SDL_Event*)(&ev)); //push a dummy event to trigger event handler
    return 0;
}

/** Thread that performs the calibration (so that we can monitor for regular SDL Events in the main thread) */
static int calThread(void*) {
    if (mouse_emu) {

    } else {
        try {
            startMimioServer(MIMIO_HOST);
        } catch (void *s) {
            fprintf(stderr, "Caught socket\n");
        } catch (...) {
            fprintf(stderr, "Caught unknown\n");
        }

        fprintf(stderr, "Waiting for mimio connection...");
        //if (MIMIO_HOST[0] != '/')
	    waitForMimioConnect();
	fprintf(stderr, " OK\n");
    }
    if (!terminate) {
        doCalibrate();
    }
    fprintf(stderr, "Cancelled\n");
    return terminate;
}

/** Initialise hardware (sockets and Mimio) and then callibrate */
int doConnectCal() {
    int ret;
    Listener *lp = 0;
    if (MIMIO_HOST[0] == '/') {
        surf = makeSurface();
    } else  {
        Listener::init();
        lp = new Listener();
        lp->start();
    }
    SDL_Thread *thr = SDL_CreateThread(calThread, 0);
    ret = SDLQueueThread(0);
    if (lp) {
        lp->stop();
    }
    int threat;
    SDL_WaitThread(thr, &threat);
    fprintf(stderr, "Joined calibrator... ");
    fprintf(stderr, "returned %d\n", threat);
    fprintf(stderr, "Joining listener [waiting for timeout]\n");
    if (lp) {     
        lp->join();
        SDLNet_Quit();
    }
    return ret;
}

/**
 * Main program of mimocal starts the callibrator.
 */
int main(int argc, char **argv) {
    fprintf(stderr, "In main()\n");
    SDL_SERVER_ADDCLIENT(MimioMonitor, MimioMonitor::port);
    if (argc > 2) {
        TARGET_SCREEN_WIDTH = atoi(argv[1]);
        TARGET_SCREEN_HEIGHT = atoi(argv[2]);
    }
    if (TARGET_SCREEN_WIDTH <= 0 ||
        TARGET_SCREEN_HEIGHT <= 0) {
        usage(argv[0]);
        return 1;
    }

    if (argc > 3)
        SIGMA = atof(argv[3]);
    if (argc > 4)
        MAXDIM = atoi(argv[4]);
    if (argc > 5)
        MIMIO_HOST = argv[5];

    if (MIMIO_HOST == std::string("mouse")) {
        mouse_emu = true;
        mouse_sem = SDL_CreateSemaphore(0);
    }

    usage(argv[0]);

    /* initialize SDL */
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        fprintf( stderr, "Video initialization failed: %s\n", SDL_GetError( ) );
	return 0;
    }

    int r = doConnectCal();

    fprintf(stderr, "Back in main() with status %d -- Calling SDL_Quit()\n", r);
    /*
     * SDL_Quit() SHOULD be called here, but SDL_net crashes sometimes.
     * Let's do an unclean exit for now so that we don't confuse people with
     * an error from Windows a la "Please report this error to Microsoft"
     */
    SDL_Quit();
    //return r;
    fprintf(stderr, "Happily returning 0 from main()\n");
    return 0;
}

static SDL_Surface *makeSurface() {
    int videoFlags = 0;                  // Flags to pass to SDL_SetVideoMode
    const SDL_VideoInfo *videoInfo;	 // this holds some info about our display
    SDL_Surface *ret;

    if (!(videoInfo = SDL_GetVideoInfo( ))) {
        fprintf( stderr, "Video query failed: %s\n", SDL_GetError( ) );
        return 0;
    }
    /* the flags to pass to SDL_SetVideoMode */
//    videoFlags  = SDL_OPENGL;          /* Enable OpenGL in SDL */
//    videoFlags |= SDL_GL_DOUBLEBUFFER; /* Enable double buffering */
    videoFlags |= SDL_HWPALETTE;       /* Store the palette in hardware */
//    videoFlags |= SDL_RESIZABLE;       /* Enable window resizing */
//    if (TRY_FULLSCREEN)

    if (FULLSCREEN)
        videoFlags |= SDL_FULLSCREEN;

    /* This checks to see if surfaces can be stored in memory */
    videoFlags |= videoInfo->hw_available ? SDL_HWSURFACE : SDL_SWSURFACE;
    /* This checks if hardware blits can be done */
    if (videoInfo->blit_hw) videoFlags |= SDL_HWACCEL;
    /* Sets up OpenGL double buffering */
//    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

    /* Set up FSAA */
    /*
    if (RConfig::TARGET_FSAA > 0) {
        int fsaa = RConfig::TARGET_FSAA;
        while (fsaa > 0 && SDL_GL_SetAttribute (SDL_GL_SAMPLES_SIZE, fsaa)
            --fsaa;
        fprintf(stderr, "Using %dx FSAA\n", fsaa);
    }*/

    /* get a SDL surface */
    ret = SDL_SetVideoMode( TARGET_SCREEN_WIDTH, TARGET_SCREEN_HEIGHT, TARGET_SCREEN_BPP, videoFlags );
    if (!ret) {
        fprintf( stderr,  "Video mode set failed: %s\n", SDL_GetError( ) );
	return 0;
    }
    if (videoFlags & SDL_HWSURFACE) {
        fprintf(stderr, "Using hardware accelleration\n");
    }

    background = SDL_LoadBMP("background.bmp");
    if (!background)
        fprintf(stderr, "Couldn't load background: %s\n", SDL_GetError());
    return ret;
}

extern "C" {
/** If you link with a fortran compiler, it will expect this */
int MAIN__ () {
    return 1;
}
}
