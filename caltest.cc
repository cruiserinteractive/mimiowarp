/* $Header: /home/tapted/cvstemp/cvs/Nightingale/mimiowarp/caltest.cc,v 7.1 2005/03/08 04:53:42 tapted Exp $ */

/**\file caltest.cc
 * A screen calibration program using / demonstrator of
 * an exact-interpolation radial basis function neural network.
 *
 * \author Trent Apted <tapted@it.usyd.edu.au>
 * \version $Revision: 7.1 $
 * \date $Date$
 */


#include <SDL.h> //for SDL_Surface, events, etc.
#include <unistd.h>  //for size_t, usleep

#include <deque>    //for points queue
#include <vector>
#include <utility>  //for std::pair
#include <fstream>
#include <iostream>


/** #define VDEBUG for verbose debugging in rbn_solve.h */
//#define VDEBUG
//#define DEBUG
#include "exactrbn/src/rbn_solve.h"
#include "strokereader.h"

using std::make_pair;

/* Configuration namespace */
namespace Config {
    int TARGET_SCREEN_WIDTH = 1024; ///< Width of the SDL surface
    int TARGET_SCREEN_HEIGHT = 768; ///< Height of the SDL Surface
    int BOX_SIZE = 20;       ///< Width of the calibration squares
    int TARGET_SCREEN_BPP = 24; ///< Colour depth of the SDL Surface
    int DIFF = 32;           ///< Distance between calibration squares
    float FROM_EDGE = 0.05;  ///< Portion from edge of screen at which calibrations squares are drawn
    bool FULLSCREEN = true;  ///< Whether to enter fullscreen mode
    bool SHOWPIXELS = true;  ///< Whether to show mouse movement events to stderr
    int MAXDIM = 9;          ///< Maximum number of calibration points
    double SIGMA = 10;       ///< Value of sigma for the RBF Network

    int MIMIO_WIDTH = 10000; ///< Width of the Mimio coordinates
    int MIMIO_HEIGHT = 5000; ///< Height of the Mimio coordindates

    int POINTS_TO_AVERAGE = 100; ///< How many mimio points to average for each calibration point
}
using namespace Config;

/** define LATTICEPATTERN to produce a line grid, rather than dots */
#define LATTICEPATTERN

static SDL_Surface *surf; ///< The SDL surface / screen display handle

static SDL_Surface *makeSurface();


static void usage(const char* argv0) {
    fprintf(stderr, "Usage: %s [<width=%d> <height=%f> [sigma=%d [numpoints=%d] ] ]\n",
            argv0,
            TARGET_SCREEN_WIDTH,
            TARGET_SCREEN_HEIGHT,
            SIGMA,
            MAXDIM);
}


/**
 * Main function - starts mimicing pen strokes.
 */
int main(int argc, char **argv) {
    atexit(&SDL_Quit);
    if (argc > 2) {
        TARGET_SCREEN_WIDTH = atoi(argv[1]);
        TARGET_SCREEN_HEIGHT = atoi(argv[2]);
    }
    if (TARGET_SCREEN_WIDTH <= 0 ||
        TARGET_SCREEN_HEIGHT <= 0) {
        usage(argv[0]);
        return 1;
    }

    startMimioServer();

    fprintf(stderr, "Waiting for mimio connection...\n");
    waitForMimioConnect();

    fprintf(stderr, "Connected. Press Enter.\n");
    getchar();
    surf = makeSurface();

    if (!surf) {
        return 2;
    }

    if (argc > 3)
        SIGMA = atof(argv[3]);
    if (argc > 4)
        MAXDIM = atoi(argv[4]);

    usage(argv[0]);

//    if (argc > 1 && strcmp(argv[1], "-t") == 0)
//        return replay();

    /* four corners */
//    XYInterp ts(SIGMA);
    XYInterp ts("network.dat");

    const Uint32 BLACK = SDL_MapRGB(surf->format, 0, 0, 0);
    const Uint32 WHITE = SDL_MapRGB(surf->format, 255, 255, 255);
    const Uint32 CYAN = SDL_MapRGB(surf->format, 0, 255, 255);
    const Uint32 GREEN = SDL_MapRGB(surf->format, 0, 255, 0);
    const Uint32 YELLOW = SDL_MapRGB(surf->format, 255, 255, 0);
    const Uint32 RED = SDL_MapRGB(surf->format, 255, 0, 0);
    const Uint32 MAGENTA = SDL_MapRGB(surf->format, 255, 0, 255);

    SDL_Rect r;
    r.w = 2;
    r.h = 2;

    MimioEvent *me;
    while ((me = waitOldMimio())) {
        //if  (me.state == MimioEvent::MOVING) {
            XYInterp::INVEC in;
            in[0] = 1.0*me->x/MIMIO_WIDTH;
            in[1] = 1.0*me->y/MIMIO_HEIGHT;
            XYInterp::OUTVEC out;
            ts.sub(out, in);
            r.x = static_cast<Sint16>(surf->w*out[0] + 0.5);
            r.y = static_cast<Sint16>(surf->h*out[1] + 0.5);
            switch (me->user) {
                case 1: SDL_FillRect(surf, &r, WHITE); break;
                case 2: SDL_FillRect(surf, &r, CYAN); break;
                case 3: SDL_FillRect(surf, &r, GREEN); break;
                case 4: SDL_FillRect(surf, &r, MAGENTA); break;
                case 5: SDL_FillRect(surf, &r, BLACK); break;
                case 6: SDL_FillRect(surf, &r, YELLOW); break;
            }
            SDL_UpdateRects(surf, 1, &r);
            delete me;
        //}
    }
}

static SDL_Surface *makeSurface() {
    int videoFlags = 0;                  // Flags to pass to SDL_SetVideoMode
    const SDL_VideoInfo *videoInfo;	 // this holds some info about our display
    SDL_Surface *ret;

    /* initialize SDL */
    if ( SDL_Init( SDL_INIT_VIDEO ) < 0 ) {
        fprintf( stderr, "Video initialization failed: %s\n", SDL_GetError( ) );
	return 0;
    }
    if (!(videoInfo = SDL_GetVideoInfo( ))) {
        fprintf( stderr, "Video query failed: %s\n", SDL_GetError( ) );
        return 0;
    }
    /* the flags to pass to SDL_SetVideoMode */
//    videoFlags  = SDL_OPENGL;          /* Enable OpenGL in SDL */
//    videoFlags |= SDL_GL_DOUBLEBUFFER; /* Enable double buffering */
    videoFlags |= SDL_HWPALETTE;       /* Store the palette in hardware */
//    videoFlags |= SDL_RESIZABLE;       /* Enable window resizing */
//    if (TRY_FULLSCREEN)

    if (FULLSCREEN)
        videoFlags |= SDL_FULLSCREEN;

    /* This checks to see if surfaces can be stored in memory */
    videoFlags |= videoInfo->hw_available ? SDL_HWSURFACE : SDL_SWSURFACE;
    /* This checks if hardware blits can be done */
    if (videoInfo->blit_hw) videoFlags |= SDL_HWACCEL;
    /* Sets up OpenGL double buffering */
//    SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );

    /* Set up FSAA */
    /*
    if (RConfig::TARGET_FSAA > 0) {
        int fsaa = RConfig::TARGET_FSAA;
        while (fsaa > 0 && SDL_GL_SetAttribute (SDL_GL_SAMPLES_SIZE, fsaa)
            --fsaa;
        fprintf(stderr, "Using %dx FSAA\n", fsaa);
    }*/

    /* get a SDL surface */
    ret = SDL_SetVideoMode( TARGET_SCREEN_WIDTH, TARGET_SCREEN_HEIGHT, TARGET_SCREEN_BPP, videoFlags );
    if (!ret) {
        fprintf( stderr,  "Video mode set failed: %s\n", SDL_GetError( ) );
	return 0;
    }
    if (videoFlags & SDL_HWSURFACE) {
        fprintf(stderr, "Using hardware accelleration\n");
    }
    return ret;
}
