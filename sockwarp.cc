/* $Header: /home/tapted/cvstemp/cvs/Nightingale/mimiowarp/sockwarp.cc,v 7.1 2005/03/08 04:53:43 tapted Exp $ */
#include <cc++/socket.h>
#include <string>
#include <iostream>

std::istream& readline(std::string& s, std::istream& i) {
    std::istream::char_type c;
    while (i.get(c) && c != '\n')
        s += c;
    return i;
}
    

int main() {
    ost::TCPSocket server("localhost", 1512);
    ost::tpport_t port;
    std::cerr << "Listening on " << server.getLocal(&port) << ":" << port << std::endl;
    while (true /*server.isActive()*/) {
        ost::tcpstream client(server);
	std::cerr << "Accepted " << client.getIPV4Peer(&port) << ":" << port;
	std::cerr << " on " << client.getIPV4Local(&port) << ":" << port << std::endl;
        std::string s;
        while (readline(s, client)) {
            int user;
	    short x, y;
//	    std::cerr << s << std::endl;
	    if (sscanf(s.c_str(), "%d UP:(%hd, %hd)", &user, &x, &y) == 3) {
	        std::cout << "UP   " << user << "(" << x << ", " << y << ")" << std::endl;
            } else if (sscanf(s.c_str(), "%d DOWN:(%hd, %hd)", &user, &x, &y) == 3) {
	        std::cout << "DOWN " << user << "(" << x << ", " << y << ")" << std::endl;
            } else if (sscanf(s.c_str(), "%d MOVE:(%hd, %hd)", &user, &x, &y) == 3) {
	        std::cout << "MOVE " << user << "(" << x << ", " << y << ")" << std::endl;
            } else {
	        std::cerr << "\nBad format" << std::endl;
	    }
            s = "";
        }
        std::cerr << "Client disconnected" << std::endl;
    }
    return 0;
}
	
